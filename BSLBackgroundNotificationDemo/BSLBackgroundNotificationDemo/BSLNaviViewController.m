//
//  BSLNaviViewController.m
//  BSLBackgroundNotificationDemo
//
//  Created by tianyin luo on 14-10-27.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLNaviViewController.h"

@interface BSLNaviViewController ()

@end

@implementation BSLNaviViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(UIColor*)themeColor
{
    return [UIColor colorWithRed:229.0/255.0
                           green:23.0/255.0
                            blue:53.0/255.0
                           alpha:1.0];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.navigationBar setBarStyle:UIBarStyleDefault];
    [self.navigationBar setBarTintColor:[self themeColor]];
    [self.navigationBar setTintColor:[UIColor whiteColor]];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    self.navigationBar.titleTextAttributes = dict;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
