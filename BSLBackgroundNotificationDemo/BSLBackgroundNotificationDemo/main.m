//
//  main.m
//  BSLBackgroundNotificationDemo
//
//  Created by tianyin luo on 14-10-27.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BSLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BSLAppDelegate class]));
    }
}
