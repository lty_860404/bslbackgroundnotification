//
//  BSLBackgroundNotification.h
//  BSLBackgroundNotification
//
//  Created by tianyin luo on 14-10-27.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSLBackgroundNotificationView.h"

@interface BSLBackgroundNotification : NSObject

+(BSLBackgroundNotificationView*) showLoadingOnViewController:(UIViewController*)viewController
                                                     delegate:(id<BSLBackgroundNotificationDelegate>)delegate
                                                   loadingStr:(NSString*)loadingStr
                                             loadingFailureStr:(NSString*)loadedResultStr;

@end
