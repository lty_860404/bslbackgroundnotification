//
//  BSLBackgroundNotification.m
//  BSLBackgroundNotification
//
//  Created by tianyin luo on 14-10-27.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLBackgroundNotification.h"

static NSMutableArray* notificationsLib;

@implementation BSLBackgroundNotification

+(BSLBackgroundNotificationView*) loadViewOnViewController:(UIViewController*)viewController
{
    if(!viewController){
        return nil;
    }
    if(notificationsLib==nil){
        notificationsLib = [[NSMutableArray alloc] init];
        return nil;
    }
    for(BSLBackgroundNotificationView* notificationView in notificationsLib){
        if(viewController == notificationView.viewController){
            return notificationView;
        }
    }
    return nil;
}

+(void) addToNotificationsLib:(BSLBackgroundNotificationView*)notificationView
{
    if(notificationsLib==nil){
        notificationsLib = [[NSMutableArray alloc] init];
    }
    [notificationsLib addObject:notificationView];
}

+(void) removeNotificationView:(BSLBackgroundNotificationView*)notificationView
{
    [notificationsLib removeObject:notificationView];
}

+(BSLBackgroundNotificationView*) showLoadingOnViewController:(UIViewController*)viewController
                                                     delegate:(id<BSLBackgroundNotificationDelegate>)delegate
                                                   loadingStr:(NSString*)loadingStr
                                             loadingFailureStr:(NSString*)loadedErrorStr
{
    if(!viewController){
        return nil;
    }
    BSLBackgroundNotificationView* notificationView = [BSLBackgroundNotification loadViewOnViewController:viewController];
    if(!notificationView){
        NSLog(@"New Notification On ViewController");
        notificationView = [BSLBackgroundNotificationView instanceOnViewController:viewController
                                                                          delegate:delegate
                                                                        loadingStr:loadingStr
                                                                  loadingFailureStr:loadedErrorStr];
        if([viewController isKindOfClass:[UITableViewController class]]){
            if(viewController.automaticallyAdjustsScrollViewInsets){
                UITableViewController* tableViewController = (UITableViewController*)viewController;
                [tableViewController.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1)
                                                          animated:NO]; 
                CGFloat topOffset = [self topOffset:viewController];
                NSLog(@"top offset:%f",topOffset);
                [notificationView setBounds:CGRectMake(notificationView.frame.origin.x,
                                                       notificationView.frame.origin.y+topOffset,
                                                       notificationView.frame.size.width,
                                                       notificationView.frame.size.height)];
                [viewController.view addSubview:notificationView];
            }else{
                [viewController.view addSubview:notificationView];
            }
        }else{
            [viewController.view addSubview:notificationView];
        }
        [BSLBackgroundNotification addToNotificationsLib:notificationView];
        [notificationView reload];
    }else{
        NSLog(@"Exist Notification On ViewController");
        if(loadingStr && ![loadingStr isEqualToString:notificationView.loadingStr]){
            [notificationView setLoadingStr:loadingStr];
        }
        if(loadedErrorStr && ![loadedErrorStr isEqualToString:notificationView.loadedFailureStr]){
            [notificationView setLoadedFailureStr:loadedErrorStr];
        }
        [notificationView reload];
    }
    return notificationView;
}

+(CGFloat) addStatusBarHeightToVerticalOffset:(CGFloat)verticalOffset
{
    CGSize statusBarSize = [UIApplication sharedApplication].statusBarFrame.size;
    if([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]){
        verticalOffset += statusBarSize.height;
    }else{
        BOOL isPortrait = UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation);
        CGFloat offset = isPortrait?statusBarSize.height:statusBarSize.width;
        verticalOffset+=offset;
    }
    return verticalOffset;
}

+(CGFloat) topOffset:(UIViewController*)viewController{
    CGFloat verticalOffset = 0.0f;
    
    if([viewController isKindOfClass:[UINavigationController class]]
       ||[viewController.parentViewController isKindOfClass:[UINavigationController class]]){
        UINavigationController* currentNavigationController;
        
        if([viewController isKindOfClass:[UINavigationController class]]){
            currentNavigationController = (UINavigationController*)viewController;
        }else{
            currentNavigationController = (UINavigationController*)viewController.parentViewController;
        }
        if(![currentNavigationController.navigationBar isHidden]){
            verticalOffset = currentNavigationController.navigationBar.frame.size.height;
        }
    }
    
    verticalOffset = [self addStatusBarHeightToVerticalOffset:verticalOffset];
    
    return verticalOffset;
}

@end
