//
//  BSLBackgroundNotificationView.h
//  BSLBackgroundNotification
//
//  Created by tianyin luo on 14-10-27.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BSLBackgroundNotificationDelegate <NSObject>

-(BOOL) reloadAction;

-(BOOL) showLoading;

@end

@interface BSLBackgroundNotificationView : UIView

@property (readonly,nonatomic) id<BSLBackgroundNotificationDelegate> backgroundNotificationDelegate;

@property (strong,nonatomic) NSString* loadingStr;

@property (strong,nonatomic) NSString* loadedFailureStr;

@property (strong,nonatomic) UIViewController* viewController;

@property (nonatomic) BOOL isLoading;

+(instancetype) instanceOnViewController:(UIViewController*)viewController
                                delegate:(id<BSLBackgroundNotificationDelegate>)delegate
                              loadingStr:(NSString*)loadingStr
                        loadingFailureStr:(NSString*)loadedResultStr;

-(void) showReloadWithText:(NSString*)text
           loadingAnimated:(BOOL)animated;

-(void) hideLoadingView;

-(void) showReloadViewWithText:(NSString*)text;

-(void) reload;

@end