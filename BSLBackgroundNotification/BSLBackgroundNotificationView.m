//
//  BSLBackgroundNotificationView.m
//  BSLBackgroundNotification
//
//  Created by tianyin luo on 14-10-27.
//  Copyright (c) 2014年 DODOPIPE LIMITED. All rights reserved.
//

#import "BSLBackgroundNotificationView.h"
#import "BSLBackgroundNotification.h"
#define BackgroundNotificationBundleName @"BSLBackgroundNotification"

@interface BSLBackgroundNotification()
    +(void) removeNotificationView:(BSLBackgroundNotificationView*)notificationView;
@end

@interface BSLBackgroundNotificationView()

@property (strong,nonatomic) id<BSLBackgroundNotificationDelegate> backgroundNotificationDelegate;
@property (strong,nonatomic) UIActivityIndicatorView* activityIndicatorView;
@property (strong,nonatomic) UILabel *loadingLabel;
@property (strong,nonatomic) UIButton* reloadBtn;

@end

@implementation BSLBackgroundNotificationView

+(instancetype) instanceOnViewController:(UIViewController*)viewController
                                delegate:(id<BSLBackgroundNotificationDelegate>)delegate
                              loadingStr:(NSString*)loadingStr
                        loadingFailureStr:(NSString*)loadedResultStr
{
    BSLBackgroundNotificationView* view =
    [[BSLBackgroundNotificationView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    view.viewController = viewController;
    view.backgroundNotificationDelegate = delegate;
    view.loadingStr = loadingStr;
    view.loadedFailureStr = loadedResultStr;
    if(![view showLoadingFlag]){
        view.alpha = 0.f;
        view.userInteractionEnabled = NO;
    }
    return view;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(UIImage*) bundleImageForFileName:(NSString*)fileName
{
    if(fileName==nil){
        return nil;
    }
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@.bundle/%@",BackgroundNotificationBundleName,fileName]];
}

-(void) reload
{
    if(self.isLoading){
        NSLog(@"isLoading,so return");
        return;
    }
    [self reloadBtnClicked:self.reloadBtn];
}

-(IBAction)reloadBtnClicked:(id)sender
{
    if(self.backgroundNotificationDelegate){
        [self setIsLoading:YES];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showReloadWithText:self.loadingStr loadingAnimated:YES];
        });
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
        dispatch_async(queue, ^{
            BOOL isSuccess = [self.backgroundNotificationDelegate reloadAction];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(isSuccess){
                    [self hideLoadingView];
                }else{
                    [self showReloadViewWithText:self.loadedFailureStr];
                }
                [self setIsLoading:NO];
            });
        });
    }
}

-(void) setup
{
    self.backgroundColor = [UIColor colorWithRed:246/255.f
                                           green:246/255.f
                                            blue:246/255.f
                                           alpha:1.f];
    _activityIndicatorView =
    [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityIndicatorView.alpha = 0.f;
    
    _loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), 44)];
    self.loadingLabel.alpha = 0.f;
    self.loadingLabel.textColor = [UIColor grayColor];
    
    _reloadBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,0,100,100)];
    self.reloadBtn.alpha=0.f;
    [self.reloadBtn setBackgroundImage:[self bundleImageForFileName:@"refresh@2x.png"]
                              forState:UIControlStateNormal];
    [self.reloadBtn addTarget:self action:@selector(reloadBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.activityIndicatorView];
    [self addSubview:self.loadingLabel];
    [self addSubview:self.reloadBtn];
    
}

- (void)dealloc {
    NSLog(@"dealloc");
    self.backgroundNotificationDelegate = nil;
    [self stopActivityIndicatorView];
    self.activityIndicatorView = nil;
    self.loadingLabel = nil;
    self.reloadBtn = nil;
}

-(void) stopActivityIndicatorView{
    if([self.activityIndicatorView isAnimating]){
        [self.activityIndicatorView stopAnimating];
    }
}

-(void) hideAllUI
{
    self.activityIndicatorView.alpha=0.f;
    self.loadingLabel.alpha=0.f;
    self.reloadBtn.alpha=0.f;
}

-(BOOL) showLoadingFlag
{
    BOOL showLoadingFlag = YES;
    if ([self.backgroundNotificationDelegate respondsToSelector:@selector(showLoading)]) {
        showLoadingFlag = [self.backgroundNotificationDelegate showLoading];
    }
    return showLoadingFlag;
}

-(void) showReloadWithText:(NSString*)text
           loadingAnimated:(BOOL)animated
{
    [self hideAllUI];
    if(animated){
        [self showLoadingAnimationWithText:text];
    }else{
        [self showPromptViewWithText:text];
    }
}

-(CGRect) loadingLabelFrameForWidth:(CGFloat)width
{
    CGRect loadingLabelFrame = self.loadingLabel.frame;
    loadingLabelFrame.size.width = width;
    loadingLabelFrame.origin.x=0;
    loadingLabelFrame.origin.y=CGRectGetHeight(self.bounds)/2.0-CGRectGetHeight(loadingLabelFrame)/2.0;
    return loadingLabelFrame;
}

-(void)showPromptViewWithText:(NSString*)promptStr
{
    [self resetAndUnableTableScroll:NO];
    [self stopActivityIndicatorView];
    
    self.loadingLabel.frame = [self loadingLabelFrameForWidth:CGRectGetWidth(self.bounds)];
    self.loadingLabel.textAlignment = NSTextAlignmentCenter;
    self.loadingLabel.text = promptStr;
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.activityIndicatorView.alpha = 0.f;
                         self.reloadBtn.alpha = 0.f;
                         self.loadingLabel.alpha = 1.f;
                         self.alpha = 1.f;
                     }
                     completion:^(BOOL finished) {
                         self.userInteractionEnabled = YES;
                     }];
    
}

-(void)showLoadingAnimationWithText:(NSString*)loadingStr
{
    if([self showLoadingFlag]){
        [self resetAndUnableTableScroll:NO];
        self.userInteractionEnabled = YES;
    }else{
        [self resetAndUnableTableScroll:YES];
        self.userInteractionEnabled = NO;
    }
    CGSize textSize = [loadingStr sizeWithFont:self.loadingLabel.font];
    CGSize indicatorViewSize = [self.activityIndicatorView frame].size;
    
    CGRect loadingLabelFrame = CGRectMake((CGRectGetWidth(self.bounds)-textSize.width+indicatorViewSize.width)/2.0,
                                          (CGRectGetHeight(self.bounds)-textSize.height)/2.0,
                                          CGRectGetWidth(self.bounds),
                                          textSize.height);
    self.loadingLabel.textAlignment = NSTextAlignmentLeft;
    self.loadingLabel.frame = loadingLabelFrame;
    self.loadingLabel.text = loadingStr;
    
    CGPoint activityIndicatorViewCenter = CGPointMake(loadingLabelFrame.origin.x-indicatorViewSize.width/2.0-2.0,
                                                      self.loadingLabel.center.y);
    self.activityIndicatorView.center = activityIndicatorViewCenter;
    [self.activityIndicatorView startAnimating];
    
    [UIView animateWithDuration:0.1
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         if(![self showLoadingFlag]){
                             self.alpha = 0.f;
                         }else{
                             self.alpha=1.f;
                         }
                         self.reloadBtn.alpha = 0.f;
                         self.loadingLabel.alpha = 1.f;
                         self.activityIndicatorView.alpha = 1.f;
                     } completion:^(BOOL finished) {
                         
                     }];
}

-(void) hideLoadingView
{
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.alpha = 0.f;
                     } completion:^(BOOL finished) {
                         if(self.viewController && [self.viewController isKindOfClass:[UITableViewController class]]){
                             NSLog(@"scrolling Enable");
                             UITableViewController* tableViewController = (UITableViewController*)self.viewController;
                             [tableViewController.tableView setScrollEnabled:YES];
                         }
                         NSLog(@"remove notification");
                         [BSLBackgroundNotification removeNotificationView:self];
                         [self removeFromSuperview];
                     }];
}

-(void) showReloadViewWithText:(NSString *)text
{
    [self resetAndUnableTableScroll:NO];
    [self hideAllUI];
    [self stopActivityIndicatorView];
    
    CGRect loadingLabelFrame = [self loadingLabelFrameForWidth:CGRectGetWidth(self.bounds)];
    self.loadingLabel.textAlignment = NSTextAlignmentCenter;
    self.loadingLabel.frame = loadingLabelFrame;
    self.loadingLabel.text = text;
    
    CGPoint reloadBtnCenter = CGPointMake(CGRectGetWidth(self.bounds)/2.0,
                                          self.loadingLabel.center.y-CGRectGetHeight(self.loadingLabel.frame)/2.0-CGRectGetHeight(self.reloadBtn.frame)/2.0-10.0);
    self.reloadBtn.center = reloadBtnCenter;
    
    [UIView animateWithDuration:0.1f
                          delay:0.f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.activityIndicatorView.alpha = 0.0;
                         self.reloadBtn.alpha = 1.f;
                         self.loadingLabel.alpha = 1.f;
                         self.alpha = 1.0;
                     } completion:^(BOOL finished) {
                         self.userInteractionEnabled = YES;
                     }];
}

-(void) resetAndUnableTableScroll:(BOOL)scrollEnable
{
    if(self.viewController && [self.viewController isKindOfClass:[UITableViewController class]]){
        NSLog(@"scroll unable");
        UITableViewController* tableViewController = (UITableViewController*)self.viewController;
        [tableViewController.tableView scrollRectToVisible:CGRectMake(0,0,1,1)
                                                  animated:NO];
        [tableViewController.tableView setScrollEnabled:scrollEnable];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
